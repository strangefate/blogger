### Package Installation
This module has some quick templates for adding a blog to your Laravel site. It includes support for CKEditor, a WYSIWYG editor with the ability to directly attach images to your post. The CKEditor requires Vue.js, but you are free to customize your project to exclude this. There is also a watered down editor with a basic text editor if you do not wish to use CKEditor.

Require package
```php
composer require strangefate/blogger
```

Run migration to add the required table for comments, likes and reports to the database
```php
php artisan migrate
```

You can public the config file to overwrite some of the basic configuration, including the slug for post model.
```php
php artisan vendor:publish --provider=StrangeFate\Blogger\BloggerServiceProvider --tag=config
```


### CKEditor (WYSIWYG)
To make use of CKEditor, run this artisan command which will automate most of the installation for you.
```php
php artisan blogeditor:install
```

This will install the Vue CKEditor module through npm and add most of the modules required. You will still need to add a javascript processor to upload images attached or dragged to the editor. To do this, add a method to the default Vue instance with the path for the image upload form.
```javascript
import CustomUploadAdapter from './classes/ckeditor-upload-adapter-class'; //added by blogeditor:install

const app = new Vue({
    el: '#app',
    mounted() {},

    // This code will be inserted into your app.js file. Customize this code for your project. You can make as many of these modules as needed for your project.
    methods: {
    	CustomUploadAdapterPlugin( editor ) {
		    editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
	    	    loader.uploadUrl = '/api/posts/editorimages';
	        	return new CustomUploadAdapter( loader );
		    }
		}
    },
    //end required code.
});
```

Because CKEditor uses HTML tags, you may not want to use blades {{}} sanitizer. Blogger has it's own sanatizer method for stripping down code. You can specify what tags are safe to keep in your project by editing the safe_tags array in the **config\blogger.php** after publishing it.
```php
'sanitizer' => [
    'safe_tags' => [
        'pre','b','em','u','ul','li','ol','p','br','tt','hr','i','table','tr','th','td',
    ]
],
```

The blogger will also resize images that are uploaded to save file space. You can customize the maximum dimensions of images in the blogger config file.
```php
'social' => [ 
    'height' => 601,
    'width' => 1147,
],
```

### Routes and Controllers
Now you will need to setup the routes and controllers for your project. Because of the multiple support cases that surround a blog (mostly from authorization), these are not done by default. Blogger does have some quick and easy Controller classes you can use to expedite this process. It is recommended that you create a new controller and inherit these classes and overwrite the methods as needed.

web.php
```php
Route::resource('/posts', 'PostController');
Route::post('/posts/{post}/comment', 'PostController@comment');
Route::post('/posts/{post}/like', 'PostController@like');
Route::get('/posts/{post}/image', 'PostController@image');
```

```php
php artisan make:controller PostController
```

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StrangeFate\Blogger\Controllers\BloggerPostWithEditorController;  //BloggerPostController without CKEdit

class PostController extends BloggerPostWithEditorController  //BloggerPostController without CKEdit
{
	public function __construct() {
        $this->middleware('auth')->except(['index','show', 'image']);
    }
}
```

For CKEditor module, there is also an API based controller to handle image uploads. Like the previous example, you should create a new controller and inherit the CKEditorImageUploadController.
api.php
```php
Route::post('/posts/images', 'api\PostImageController@image_upload'); //single image upload (ImageSelector.vue)
Route::post('/posts/editorimages', 'api\PostImageController@editor_image_upload');  //CKeditor Image. This URL should match what you put in for the loader.uploadUrl in the app.js module.
```

```php
php artisan make:controller api\PostImageController
```

```php
<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use StrangeFate\Blogger\Controllers\CKEditorImageUploadController;

class PostImageController extends CKEditorImageUploadController {
   
   public function __construct() {
       $this->middleware(['auth:api']);
   }  
}
```

### Views
This project has ready to use views for rapidly customizing you're solution. It's pretty likely that you will want to over write them. To do this, publish the views to your project and format to your desire.
```php
php artisan vendor:publish --provider=StrangeFate\Blogger\BloggerServiceProvider --tag=views
```

Views
```blade
@include('blogger::index') {{-- Index layout for base overview --}}
@include('blogger::card') {{-- Basic card layout for index view --}}
@include('blogger::create') {{-- Create form with image upload --}}
@include('blogger::ck_create') {{-- Create form with text editor and image picker --}}
@include('blogger::edit') {{-- Create form with image upload --}}
@include('blogger::ck_edit') {{-- Edit form with text editor and image picker --}}
@include('blogger::show') {{-- Article view --}}
@include('blogger::templates.text-editors') {{-- Note to be used. Example text editors. --}}
```


### Comments
The Blogger uses [strangefate\commenthandler](https://packagist.org/packages/strangefate/commenthandler) for post comments and likes and will automatically be pulled in with this project. Review the comment handler documenation for further configuration details.