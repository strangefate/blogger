<?php

namespace StrangeFate\Blogger\Interfaces;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class BloggerHelperFunctions {

	public static function scriptTagRemoval($source) {

		$dom = new \DOMDocument();

		$html = "<div>$source</div>";

		libxml_use_internal_errors(true);
		$dom->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
		libxml_clear_errors();

		$script = $dom->getElementsByTagName('script');

		$remove = [];
		foreach($script as $item) { $remove[] = $item; }
		foreach ($remove as $item) { $item->parentNode->removeChild($item); }

		$results = $dom->saveHTML();

		return preg_replace('/^<[^>]+>|<\/[^>]+>$/', '', $results);
	}

	public static function sanatize_text($text) {
		//convert all html characters to remove possible malicious code
		$t = htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
		
		//un-convert safe tags back to html
		$t = preg_replace('#&lt;(/?(?:' . implode("|", config('blogger.sanitizer.safe_tags') ) . '))&gt;#', '<\1>', $t);
		$t = str_replace("&amp;nbsp;", "&nbsp;", $t);
		$t = str_replace("&lt;hr /&gt;", "<hr />", $t);
	
		//Remove blank lines
		$t = preg_replace('/<p>\W*<\/p>/i', '', $t);

		return $t != "" ? $t : null;
	}

	public static function social_scale($image) {
		$image = preg_replace('#^/(storage)#i', '', $image);
		$image = storage_path('app/public' . $image);

		if(! \File::exists($image) )
    		$image = config('blogger.defaultImage');

    	return Image::make( $image )
    		->fit( config('blogger.social.width'), config('blogger.social.height') )
    		->response();
	}
}