<?php

namespace StrangeFate\Blogger\Interfaces;

use Illuminate\Http\Resources\Json\JsonResource;

class PostSummarySchema extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $summary = json_decode($this->summary);

        return [
            "@context" => "http://schema.org",
            "@type" =>"BlogPosting",
            "@id" => url("/" . config("blogger.root_path") . "/$this->id"),
            "headline" =>  $this->title,
            "image" => isset($this->image) ? url($this->image) : url( config('blogger.defaultImage') ),
            "author" => $this->user->name,
            "datePublished" => $this->created_at->toIso8601String(),
            "dateModified" => $this->when( isset($this->updated_at) , $this->updated_at->toIso8601String() ),
            "description" => strip_tags($this->summary),
            "mainEntityOfPage" => [
                "@type" => "WebPage",
                "@id" => url("/" . config("blogger.root_path") . "/$this->id")
            ],
            "commentCount" => $this->comments->count(),
            "publisher" => [
                "@type" => "Person",
                "name" => $this->user->name,
            ]
        ];
    }
}
