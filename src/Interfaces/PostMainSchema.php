<?php

namespace StrangeFate\Blogger\Interfaces;

use Illuminate\Http\Resources\Json\JsonResource;

class PostMainSchema extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       $summary = json_decode($this->summary);

        return [
            "@context" => "http://schema.org",
            "@type" =>"BlogPosting",
            "@id" =>  url("/" . config("blogger.root_path") . "/$this->id"),
            "headline" =>  $this->title,
            "image" => isset($this->image) ? url($this->image) : url( config('blogger.defaultImage') ),
            "author" => $this->user->name,
            "datePublished" => $this->created_at->toIso8601String(),
            "dateModified" => $this->when( isset($this->updated_at) , optional($this->updated_at)->toIso8601String() ),
            "articleBody" => strip_tags($this->body),
            "mainEntityOfPage" => [
                "@type" => "WebPage",
                "@id" => url("/" . config("blogger.root_path") . "/$this->id")
            ],
            "publisher" => [
                "@type" => "Person",
                "name" => $this->user->name,
            ],
            "commentCount" => $this->comments()->active()->count(),
            "comment" => $this->comments()->active()->get()->map(function ($i) {
                return [
                    "@type" => "Comment",
                    "author" => [
                        "@type" => "Person",
                        "name" => $i->user->name,
                    ],
                    "dateCreated" => $i->created_at->toIso8601String(),
                    "text" => $i->message,
                ];
            })
        ];
    }
}