<?php

namespace StrangeFate\Blogger\Interfaces;

use StrangeFate\Blogger\Tag;

trait HasTags
{
	public function tags() {
		return $this->morphToMany(Tag::class, 'taggable');
	}
}