<?php

namespace StrangeFate\Blogger;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;
}