@extends ('layouts.master')

@section('content')
	<div class="grid-container">
		<div class="grid-x grid-margin-x small-up-2 medium-up-3">
			@forelse($posts as $post)
				@include('blogger::card', ['post' => $post])
			@empty
				<div class="callout cell">
					There are no posts!
				</div>
			@endforelse
		</div>
	</div>
@endsection

@section('title')
	<script type="application/ld+json">
	@json( \StrangeFate\Blogger\Interfaces\PostSummarySchema::collection($posts) )
	</script>
@endsection