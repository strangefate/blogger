@extends ('layouts.master')

@section('content')
<div class="grid-container">
	<div class="grid-x align-center">
		<div class="cell medium-8">
			<h3>Create a new form post</h3>

			<form method="POST" action="{{ url( config("blogger.root_path") ) }}" enctype="multipart/form-data">
				@csrf

				<div class="grid-x">

					<div class="cell">
						<label for="title">Title
							<input type="text" id="title" name="title" required value="{{ old('title') }}">
						</label>
						@include('layouts.error-field', ['fieldname' => 'title'])
					</div>

					<div class="cell">
						<label for="category_id">Category
							<select id="category_id" name="category_id" required>
								<option value="" @if( !old('category_id') ) selected @endif disabled>Please select a category</option>
								@foreach($categories as $category)
								<option value="{{$category->id}}" @if($category->id == old('title')) selected @endif>
									{{$category->name}}
								</option>
								@endforeach
							</select>
						</label>
					</div>

					<div class="cell small-7">
						Attach Image
						<label for="image" class="button expanded">Upload File</label>
						<input type="file" id="image" name="image" class="show-for-sr">
						@include('layouts.error-field', ['fieldname' => 'image'])
					</div>

					<div class="cell small-1"></div>
					
					<div class="cell small-4">
						Make post active
						<div class="switch">
							<input class="switch-input" id="isActive" type="checkbox" name="isActive" @if(old('isActive') && old('isActive') == 'on') checked @endif>
							<label class="switch-paddle" for="isActive">
								<span class="show-for-sr">Set post as active</span>
							</label>
						</div>
						@include('layouts.error-field', ['fieldname' => 'isActive'])
					</div>

					<div class="cell">
						<label for="summary">Summary
							<textarea id="summary" name="summary" rows="3"
							placeholder="A short description of your article. Will appear on search engines and social media sites when people share the link." required>{{ old('summary') }}</textarea>
							@include('layouts.error-field', ['fieldname' => 'summary'])
						</label>
					</div>

					<div class="cell">
						<label for="body">Body
							<textarea id="body" name="body" rows="5"
							placeholder="The main body of your article" required>{{ old('summary') }}</textarea>
							@include('layouts.error-field', ['fieldname' => 'title'])
						</label>
					</div>

					<div class="cell">
						<input class="button expanded small" type="submit" value="Submit">
					</div>
				</div>

			</form>

		</div>
	</div>
</div>
@endsection




@section('title')
{{-- Header information IE header,meta, style --}}
@endsection

@section('scripts')
{{-- Custom footer info. User for java scripts. NOTE: Happens after jquery is loaded so jquery commands OK! --}}
@endsection