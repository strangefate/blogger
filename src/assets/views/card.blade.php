<div class="cell">
	<div class="card">
		<div class="card-divider">
			<h4>{{ $post->title }}</h4>
		</div>
		@if($post->image)
			<img src="{{ $post->image }}">
		@endif
		<div class="card-section">
			<p class="card-details">
				By: {{ $post->user->name }} <br />
				<span class="card-date">
					{{ $post->created_at->toDayDateTimeString() }}
				</span> 
			</p>
			<p class="card-summary">{!!
			 	\StrangeFate\Blogger\Interfaces\BloggerHelperFunctions::sanatize_text($post->summary) 
			!!}</p>
		</div>
	</div>
</div>