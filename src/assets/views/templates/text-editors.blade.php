{{-- 
	THESE TEMPLATES ARE NOT FOR DIRECT USE. 
	THEY SHOULD BE COPIED AND CUSTOMIZED FOR YOUR FORM. 
--}}


{{-- Simple form layout --}}
<text-editor name="summary" header="Summary" height="250" default="{{ old('summary') ?: $post->summary }}">
	<template v-slot:errors>
		@include('layouts.error-field', ['fieldname' => 'summary'])
	</template>
</text-editor>


{{-- Text form with image and media embed support --}}
{{-- Add and configure the URL for the CustomUploadAdapterPlug in you app.js --}}
<text-editor name="body" header="Article Body" height="400" 
	default="{{ old('body') ?: $post->body }}"
	:config="{{ config('blogger.editor.advanced') }}">
	<template v-slot:errors>
		@include('layouts.error-field', ['fieldname' => 'body'])
	</template>
</text-editor>