@extends ('layouts.master')

@section('content')
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell">
				<div class="grid-x article-header align-bottom">
					<div class="cell auto">
						<h4>{{ $post->title }}</h4>
						<p>{{ $post->created_at->toDayDateTimeString() }}</p>
					</div>
				</div>

				<hr>

				<div class="content ck-content">
					{!! \StrangeFate\Blogger\Interfaces\BloggerHelperFunctions::scriptTagRemoval($post->body) !!}
				</div>
			</div>	
		</div>

		<hr>
		<h4>Comments on this post</h4>

		@include('comments::comments', [
			'comments' => $post->comments()->show()->get(),
			'url' => "/" . config("blogger.root_path") . "/$post->id/comment"
		])
	</div>

@endsection

@section('title')
{{--social media scrapping information --}}
<meta property="og:image:width" content="{{ config('blogger.social.width') }}">
<meta property="og:image:height" content="{{ config('blogger.social.height') }}">
<meta property="og:title" content="{{ $post->title }} | {{ env('APP_NAME') }}">
<meta property="og:description" content="{{ strip_tags($post->summary) }}">
<meta property="og:image" content="{{ url($post->image ?: config('blogger.defaultImage') ) }}">
<meta property="og:url" content="{{ url( config("blogger.root_path") . "/$post->id") }}">
<meta property="og:type" content="website">

<title>{{ $post->title }} | {{ env('APP_NAME') }}</title>
<meta name="description" CONTENT="{{ strip_tags($post->summary) }}">

<script type="application/ld+json">
@json( new \StrangeFate\Blogger\Interfaces\PostMainSchema($post) )
</script>

@endsection