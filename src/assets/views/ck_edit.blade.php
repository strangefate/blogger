@extends ('layouts.master')

@section('content')
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-8">
				<h3>Edit {{$post->title}}</h3>

				<form method="POST" action="{{ url( config("blogger.root_path") . "/$post->id") }}">
					@csrf
					{{ method_field('PATCH') }}

					<div class="grid-x">
						
						<div class="cell">
							<label for="title">Title
								<input type="text" id="title" name="title" required value="{{ old('title') ?: $post->title }}">
							</label>
							@include('layouts.error-field', ['fieldname' => 'title'])
						</div>
						
						<div class="cell">
							<label for="category_id">Category
								<select id="category_id" name="category_id" required>
									<option value="0" disabled>Please select an option</option>
									@foreach($categories as $category)
										<option value="{{ $category->id}}" @if($category->id == (old('category_id') ?: $post->category_id )) selected @endif>
											{{ $category->name}}
										</option>
									@endforeach
								</select>
							</label>
							@include('layouts.error-field', ['fieldname' => 'category_id'])
						</div>

						<div class="cell small-7">
							<image-select-upload 
								:images='@json($images)'
								fieldname="image"
								uploadurl="/api/{{ config("blogger.root_path") }}/images"
								default="{{ old('image') ?: $post->image }}"
								>
							</image-select-upload>
							@include('layouts.error-field', ['fieldname' => 'image'])
						</div>

						<div class="cell small-1"></div>

						<div class="cell small-4">
							Make post active
							<div class="switch">
								<input class="switch-input" id="isActive" type="checkbox" name="isActive" 
								@if( (old('isActive') && old('isActive') == 'on') || ( old('isActive') === null && $post->isActive) )
									checked
								@endif>
								<label class="switch-paddle" for="isActive">
									<span class="show-for-sr">Set post as active</span>
								</label>
							</div>
							@include('layouts.error-field', ['fieldname' => 'isActive'])
						</div>

						<div class="cell">
							<text-editor name="summary" header="Summary" height="150" default="{{ old('summary') ?: $post->summary }}">
								<template v-slot:errors>
									@include('layouts.error-field', ['fieldname' => 'summary'])
								</template>
							</text-editor>
							@include('layouts.error-field', ['fieldname' => 'summary'])
						</div>
						
						<div class="cell">
							<text-editor name="body" header="Article Body" height="400" 
								default="{{ old('body') ?: $post->body }}"
								:config="{{ config('blogger.editor.advanced') }}">
								<template v-slot:errors>
									@include('layouts.error-field', ['fieldname' => 'body'])
								</template>
							</text-editor>
						</div>
										
						<div class="cell">
							<input class="button expanded small" type="submit" value="Submit">
						</div>
					</div>
				</form>
				
			</div>			
		</div>
	</div>
@endsection

@section('title')
	{{-- Header information IE header,meta, style --}}
@endsection

@push('scripts')
	{{-- Custom footer info. User for java scripts. NOTE: Happens after jquery is loaded so jquery commands OK! --}}
@endpush