export default class CustomUploadAdapter {
	constructor( loader ) {
		this.loader = loader;
		this.url = loader.uploadUrl;
	}

	upload() {

		return  this.loader.file
			.then(file => new Promise((resolve, reject) => {
				const data = new FormData();
				data.append( 'image', file );

				axios.post(this.url ,data)
					.then(res => {
						console.log(res.data);
						resolve({ default: res.data.url });
					}).catch(error => {
						reject(error);
					});
		}) );
	}

	abort() {
		// Reject promise returned from upload() method.
	}
}