import CKEditor from '@ckeditor/ckeditor5-vue2';
Vue.use( CKEditor );

//You may need to copy these item to the app.js file to have them properly load
Vue.component('text-editor', require('./components/TextEditor.vue').default);
Vue.component('image-select-upload', require('./components/ImageSelect.vue').default);


import CustomUploadAdapter from './classes/ckeditor-upload-adapter-class';

/*
//Copy and Past this code into the methods{} section of the default Vue instance to create a custom upload function

methods: {
	CustomUploadAdapterPlugin( editor ) {
	    editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
    	    loader.uploadUrl = /api/url/for/image/upload';
        	return new CustomUploadAdapter( loader );
	    }
	}
},
*/