<?php
namespace StrangeFate\Blogger;

use Illuminate\Support\ServiceProvider;
use StrangeFate\Blogger\Commands\BloggerEditorInstall;

class BloggerServiceProvider extends ServiceProvider
{
	public function boot() {
		if( $this->app->runningInConsole() ) {
			$this->commands([
				BloggerEditorInstall::class
			]);
		}

		$this->mergeConfigFrom(__DIR__.'/config/blogger.php', 'blogger' );
		$this->loadMigrationsFrom(__DIR__.'/migrations');
		$this->loadViewsFrom(__DIR__.'/assets/views', 'blogger');

		$this->publishes([
			__DIR__.'/config/blogger.php' => config_path('blogger.php'),
		], 'config');

		$this->publishes([
			__DIR__.'/assets/views' => resource_path('views/vendor/blogger')
		], 'views');

		$this->publishes([
			__DIR__.'/assets/js' => resource_path('js'),
			__DIR__.'/assets/scss' => resource_path('sass'),
		], 'ckedit');
	}

	public function register() {

	}
}