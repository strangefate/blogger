<?php

namespace StrangeFate\Blogger;

use App\User;
use StrangeFate\Blogger\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use StrangeFate\Blogger\Interfaces\HasTags;
use StrangeFate\CommentHandler\Interfaces\HasLikes;
use StrangeFate\CommentHandler\Interfaces\HasComments;

class Post extends Model
{
    use HasTags, HasComments, HasLikes;

    public function user() {
		try {
			return $this->belongsTo(\App\Models\User::class);
		} catch (ModelNotFoundException $e) {
			return $this->belongsTo(\App\User::class);
		}
	}

    public function category() {
    	return $this->belongsTo(Category::class);
    }

    public function scopeActive($query) {
    	return $query->where('isActive', true);
    }

    protected $with = ['user', 'tags', 'category'];
    
    public function getRouteKeyName() {
        return config('blogger.slug');
    }
}