<?php

return [
	'defaultImage' => '',
	'root_path' => 'posts',
	'image_path' => 'images/posts',
	'slug' => 'id',

	'social' => [ 
		'height' => 601,
		'width' => 1147,
	],
	
	'sanitizer' => [
		'safe_tags' => [
			'pre','b','em','u','ul','li','ol','p','br','tt','hr','i','table','tr','th','td',
		]
	],

	'editor' => [
		'advanced' => "{
				mediaEmbed: { previewsInData: true },
				extraPlugins: [ CustomUploadAdapterPlugin ],
				image: {
		            toolbar: [ 'imageTextAlternative','|','imageStyle:alignLeft','imageStyle:full','imageStyle:alignRight'],
		            styles: ['full','alignLeft', 'alignRight']
		        }
			}"
	]
];