<?php

namespace StrangeFate\Blogger\Controllers;

use Illuminate\Http\Request;
use StrangeFate\Blogger\Post;
use StrangeFate\Blogger\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CKEditorImageUploadController extends Controller
{
	public function image_upload(Request $request) {
        $request->validate(['image' => 'required|image']);

        $url = $this->save_form_image($request->image);

        return [
            'name' => $request->image->getClientOriginalName(), 
            'url' => $url
        ];
    }

    public function editor_image_upload(Request $request) {
        $request->validate(['image' => 'required|image']);

        return [
            'uploaded' => true, 
            'url' => 
                $this->save_form_image($request->image, config('blogger.image_path') . "/inline")
        ];
    }

    public function save_form_image($image, $path = null) {
        $path = $path ?: config('blogger.image_path');
        $name = $image->getClientOriginalName();

        $save = $image->storeAs( "public/" . $path, $name);

        return Storage::url( $path . "/" . $name );
    }
}