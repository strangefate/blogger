<?php

namespace StrangeFate\Blogger\Controllers;

use Illuminate\Http\Request;
use StrangeFate\Blogger\Post;
use StrangeFate\Blogger\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use StrangeFate\Blogger\Events\NewBlogPostCreated;
use StrangeFate\Blogger\Interfaces\BloggerHelperFunctions;

class BloggerPostWithEditorController extends Controller
{
    public $validator = [
        'title' => 'required',
        'category_id' => 'required|exists:categories,id',
        'image' => 'nullable',
        'summary' => 'required',
        'body' => 'required',
        'isActive' => 'nullable',
    ];

    /** Display a listing of the resource. */
    public function index() {
        $posts = Post::latest()->active()->get();

        return view('blogger::index', compact('posts'));
    }

    /** Show the form for creating a new resource. */
    public function create() {
        $categories = Category::orderBy('name')->get();
        $files = collect( Storage::files( "public/" . config('blogger.image_path') ) );
        $images = $files->map(function ($i,$k) { 
            return ['name' => basename($i), 'url' => Storage::url($i)]; } );

        return view('blogger::ck_create', compact('categories', 'images') );
    }

    /** Store a newly created resource in storage. */
    public function store(Request $request) {
        $new = $request->validate($this->validator);

        $new['user_id'] = auth()->id();
        $new['isActive'] = isset($request['isActive']) && $request['isActive'] == 'on';

        $post = Post::create($new);

        //Dispatch Event for custom code to react to inside the system.
        NewBlogPostCreated::dispatch( $post );

        return redirect( config("blogger.root_path") . "/$post->id");
    }

    /** Display the specified resource. */
    public function show(Post $post) {
        return view('blogger::show', compact('post'));
    }

    /** Show the form for editing the specified resource. */
    public function edit(Post $post) {
        $categories = Category::orderBy('name')->get();
        $files = collect( Storage::files( "public/" . config('blogger.image_path') ) );
        $images = $files->map(function ($i,$k) { 
            return ['name' => basename($i), 'url' => Storage::url($i)]; } );

        return view('blogger::ck_edit', compact('post', 'categories', 'images'));
    }

    /** Update the specified resource in storage. */
    public function update(Request $request, Post $post) {
        $new = $request->validate($this->validator);

        $new['isActive'] = isset($request['isActive']) && $request['isActive'] == 'on';

        $post->update($new);

        return redirect( config("blogger.root_path") . "/$post->id");
    }

    /** Remove the specified resource from storage. */
    public function destroy(Post $post) {
        $post->delete();

        return redirect( config("blogger.root_path") );
    }

    public function comment(Request $request, Post $post) {
        $comment = $request->validate(['message' => 'required']);
        
        $comment['user_id'] = auth()->id();

        $post->comments()->create($comment);

        return redirect()->back();
    }

    public function like(Post $post) {
        $post->like( auth()->id() );

        return redirect()->back();
    }

    public function image(Post $post) {
        return BloggerHelperFunctions::social_scale($post->image);
    }

    public function save_form_image($image, $path = null) {
        $path = $path ?: config('blogger.image_path');
        $name = $image->getClientOriginalName();

        $save = $image->storeAs( "public/" . $path, $name);

        return Storage::url( $path . "/" . $name );
    }
}