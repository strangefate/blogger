<?php

namespace StrangeFate\Blogger\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BloggerEditorInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blogeditor:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command installs CKEditor for Vue.js';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = Process::fromShellCommandline('npm install --save @ckeditor/ckeditor5-vue2 @ckeditor/ckeditor5-build-classic');

        $this->info("Adding CKEditor through npm.");

        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                $this->error($buffer);
            } else {
                $this->line($buffer);
            }
        });

        $this->info("Calling command to publish views.");
        $this->call('vendor:publish', [
            '--tag' => 'ckedit'
        ]);

        file_put_contents(
            resource_path('sass/app.scss'), 
            "\n@import \"ckeditor\";",
            FILE_APPEND | LOCK_EX
        );
        
        $this->fix_app_js_file();       

        $this->info("Installation complete!");

        $this->line("");
        $this->error("Follup items");
        $this->info("Configure upload adapters and paths for editors with file drops.");
    }

    private function fix_app_js_file() {
        $js = resource_path('/js/app.js');
        $lines = array();
        foreach(file($js) as $line) {
            //checks for before writing current line
            if (strpos($line, "const app = new Vue") !== FALSE) {
                array_push($lines, "import CustomUploadAdapter from './classes/ckeditor-upload-adapter-class';\n\n");
            }

            // write current line to array
            array_push($lines, $line);

            //test for post line additions
            if (strpos($line, "require('./bootstrap');") !== FALSE) {
                array_push($lines, "require('./ckeditor-components');\n");
            }

            if (strpos($line, "el: '#app',") !== FALSE) {
                array_push($lines, "\n    methods: { \n");
                array_push($lines, "        CustomUploadAdapterPlugin( editor ) { \n");
                array_push($lines, "            editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => { \n");
                array_push($lines, "                loader.uploadUrl = '/api/posts/editorimages'; \n");
                array_push($lines, "                return new CustomUploadAdapter( loader ); \n");
                array_push($lines, "            } \n");
                array_push($lines, "        } \n");
                array_push($lines, "    }, \n");
            }
        }

        file_put_contents($js, $lines);
    }
}